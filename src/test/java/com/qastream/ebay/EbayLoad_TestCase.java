package com.qastream.ebay;

import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import org.junit.Assert; 
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class EbayLoad_TestCase {
	
	private WebDriver driver;
	By mainPageLocator = By.id("gh");
	By searchBoxLocator = By.id("gh-ac");
	By searchBtnLocator = By.id("gh-btn");
	By brandLocator = By.cssSelector("input[aria-label=\"PUMA\"]");
	By sizeLocator = By.cssSelector("input[aria-label=\"10\"]");
	By resultsLocator =  By.xpath("//h1[@class='srp-controls__count-heading']//span[@class='BOLD']");
	By orderLocator =  By.cssSelector("#w9 .srp-controls__control--legacy");
	By lowpriceLocator = By.cssSelector(".btn:nth-child(4)");
	By priceLocator = By.cssSelector("#srp-river-results-listing1 .s-item__price");
	
	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/chromedriver/chromedriver");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.ebay.com/");
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void loadMainPage() throws InterruptedException{
		Thread.sleep(2000);
		//Validar si la pàgina carga
		if(driver.findElement(mainPageLocator).isDisplayed()){
			System.out.print("\n-----------------------");
			System.out.print("\nLa página ha cargado."); 
			System.out.print("\n-----------------------");
			//Buscar por key "shoes"
			driver.findElement(searchBoxLocator).sendKeys("shoes");
			driver.findElement(searchBtnLocator).click();
			System.out.print("\n-----------------------");
			System.out.print("\nBusca Zapatos.");
			System.out.print("\n-----------------------");
			Thread.sleep(2000);
			//Filtrar por brand "Puma"
			driver.findElement(brandLocator).click();
			System.out.print("\n-----------------------");
			System.out.print("\nPUMA ha sido seleccionado.");
			System.out.print("\n-----------------------");
			Thread.sleep(2000);
			//Capturar los resultados totales
			String totalResultsBefore = driver.findElement(resultsLocator).getText();
			System.out.print("\n-----------------------");
			System.out.print("\nTotalResults ha sido revisado -- "+totalResultsBefore);
			System.out.print("\n-----------------------");
			Thread.sleep(2000);
			//Filtrar por size "10"
			driver.findElement(sizeLocator).click();
			System.out.print("\n-----------------------");
			System.out.print("\nSize 10 ha sido seleccionado.");
			System.out.print("\n-----------------------");
			Thread.sleep(2000);
			//Capturar los resultados filtrados
			String totalResultsAfter = driver.findElement(resultsLocator).getText();
			System.out.print("\n-----------------------");
			System.out.print("\nTotalResults ha sido revisado -- "+totalResultsAfter);
			System.out.print("\n-----------------------");
			Thread.sleep(2000);
			Assert.assertNotEquals("\nThe quantity of elements should be different. ", totalResultsAfter, totalResultsBefore);
			//Almacenar primer grupo de resultados
			System.out.print("\n---------- Elementos ordenados por Mejor Resultado -------------");
			System.out.print("\n-----------------------");
			Hashtable<String,String> itemsbybestresult=new Hashtable<String,String>();
			for (int i = 1; i<6; i++) {
				String indexprice = "#srp-river-results-listing"+i+" .s-item__price";
				String indexname = "#srp-river-results-listing"+i+" .s-item__title";
				String key = driver.findElement(By.cssSelector(indexname)).getText();
				String value = driver.findElement(By.cssSelector(indexprice)).getText();
				itemsbybestresult.put(key, value);
				System.out.print("\n Item: "+key);
				System.out.print(" Price: "+value);
			}
			System.out.print("\n-----------------------");
			//Ordenar por menor precio
			Actions builder = new Actions(driver);
			WebElement element = driver.findElement(orderLocator);
			builder.moveToElement(element).build().perform();
			System.out.print("\n-----------------------");
			System.out.print("\nOrder bt ha sido seleccionado.");
			System.out.print("\n-----------------------");
			Thread.sleep(2000);
			driver.findElement(lowpriceLocator).click();
			System.out.print("\n-----------------------");
			System.out.print("\nLow Price ha sido seleccionado.");
			System.out.print("\n-----------------------");
			Thread.sleep(2000);
			//Almacenar grupo de resultados ordenado
			System.out.print("\n---------- Elementos ordenados por Precio mas bajo primero -------------");
			System.out.print("\n-----------------------");
			Hashtable<String,String> itemsbylowprice=new Hashtable<String,String>();
			for (int i = 1; i<6; i++) {
				String indexprice = "#srp-river-results-listing"+i+" .s-item__price";
				String indexname = "#srp-river-results-listing"+i+" .s-item__title";
				String indexshipping = "#srp-river-results-listing"+i+" .s-item__shipping";
				String key = driver.findElement(By.cssSelector(indexname)).getText();
				String price = driver.findElement(By.cssSelector(indexprice)).getText();
				String shipping = driver.findElement(By.cssSelector(indexshipping)).getText();
				itemsbylowprice.put(key, price);
				System.out.print("\n Item: "+key);
				System.out.print(" Price: "+price+" Shipping: "+shipping);
			}
			System.out.print("\n-----------------------");
			Assert.assertFalse("\nDifferent elements have been deployed",itemsbybestresult.equals(itemsbylowprice) );
			// Ordenar items por nombre.
		    Vector<String> keysfromLowp = new Vector<String>(itemsbylowprice.keySet());
		    Collections.sort(keysfromLowp);
		    Iterator<String> it = keysfromLowp.iterator();
		    System.out.print("\n----------- 5 primeros elementos ordenados por Nombre ------------");
		    System.out.print("\n-----------------------");
		    while (it.hasNext()) {
		       String itemfromLowp =  (String)it.next();
		       System.out.println("\n "+itemfromLowp + " " + (String)itemsbylowprice.get(itemfromLowp));
		    }
		    System.out.print("\n-----------------------");
		}
		else {
			System.out.print("Ebay Page was not found");
		}
	}
	//Pending to check for different options.
	/*
	public int getprice(String cadena) throws Exception {
		int price=0;
		Pattern patron = Pattern.compile("/($[0-9,]+(.[0-9]{2})?)/");
		Matcher encaja = patron.matcher(cadena);
		//----- /($[0-9,]+(.[0-9]{2})?)/
		return price;
	}*/
}


